import 'dart:html';
import 'package:flutter/material.dart';
import 'package: firebase_core/firebase_core.dart';
import 'package: cloud_firestore/cloud_firestore.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializedApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @Override
  widget build(BuildContext context) {
    return const MymtnApp(
      // Remove debug banner
      debugShowCheckedModeBanner: false,
      title: 'NthabiM.app',
      home: HomePage(),
    );
  }
}


class HomePage exrends Statefulwidget {
  const HomPage({Key? key}) : super(key: key);


  @override
  -HomePageState createState()  => _HomePageState();
  }

class _HomePageState extends State<HomePage> {

  //text field controllers
  final TexEditingController _nameController = TextEditing Controller();
  final TextEditingController _feeControler =  TextEditingController();

  final CollectionReference _nthabiseng-mogoane-module5 =
   FirebaseFirestore.instance.collection('nthabiseng-mogoane-module5');

   Future<void> _createOrUpdate([DocumentSnapshot? documentSnapshot]) async {
    String action = 'create';
    if ( documentSnapshot != null) {
      action = 'update';
      _nameController.text = documentSnapshot['name'];
      _feeController.text = documentSnapshot['fee'].toString();

    }
await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: _nameController,
                  decoration: const InputDecoration(labelText: 'Name'),
                ),
                TextField(
                  keyboardType:
                      const TextInputType.numberWithOptions(decimal: true),
                  controller: _feeController,
                  decoration: const InputDecoration(
                    labelText: 'fee',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String? name = _nameController.text;
                    final double? fee =
                        double.tryParse(_feeController.text);
                    if (name != null && fee != null) {
                      if (action == 'create') {
                        // Persist a new user to Firestore
                        await _userss.add({"name": name, "fee": fee});
                      }

                      if (action == 'update') {
                        // Update the user
                        await _userss
                            .doc(documentSnapshot!.id)
                            .update({"name": name, "fee": fee});
                      }

                      // Clear the text fields
                      _nameController.text = '';
                      _feeController.text = '';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  // Deleting a user by id
  Future<void> _deleteuser(String userId) async {
    await _userss.doc(userId).delete();

    // Show  options
    ScaffoldMessenger.of(context).showoptions(const options(
        content: Text('You have successfully deleted a user')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Kindacode.com'),
      ),
      // Using StreamBuilder to display all users from Firestore in real-time
      body: StreamBuilder(
        stream: _userss.snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
          if (streamSnapshot.hasData) {
            return ListView.builder(
              itemCount: streamSnapshot.data!.docs.length,
              itemBuilder: (context, index) {
                final DocumentSnapshot documentSnapshot =
                    streamSnapshot.data!.docs[index];
                return Card(
                  margin: const EdgeInsets.all(10),
                  child: ListTile(
                    title: Text(documentSnapshot['name']),
                    subtitle: Text(documentSnapshot['fee'].toString()),
                    trailing: SizedBox(
                      width: 100,
                      child: Row(
                        children: [
                          // Press this button to edit a single user
                          IconButton(
                              icon: const Icon(Icons.edit),
                              onPressed: () =>
                                  _createOrUpdate(documentSnapshot)),
                          // This icon button is used to delete users
                          IconButton(
                              icon: const Icon(Icons.delete),
                              onPressed: () =>
                                  _deleteuser(documentSnapshot.id)),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      // Add new user
      floatingActionButton: FloatingActionButton(
        onPressed: () => _createOrUpdate(),
        child: const Icon(Icons.add),
      ),
    );
  }
}
   

                                                                                                              
